CUR_DIR := $(shell pwd)
BUILD_DIR := $(CUR_DIR)/build
COMPILE_FLAGS := $(shell root-config --cflags) -O3 -fPIC
BUILD_FLAGS := $(shell root-config --libs) -L../tools/build -lTools 

SOURCES := $(wildcard *.cc)
OBJECTS := $(addprefix $(BUILD_DIR)/, $(SOURCES:.cc=.o))
HEADERS := $(wildcard *.h)

.PHONY:	obj  all	clean	show_vars

all:	$(BUILD_DIR)/test

$(BUILD_DIR)/test:				$(BUILD_DIR)/test2.o
	g++ $(BUILD_FLAGS) -o $@  $^ 

obj:	$(BUILD_DIR)	$(OBJECTS)

$(BUILD_DIR)/%.o:	%.cc	$(HEADERS)
	g++ -c -o $@ $(COMPILE_FLAGS) $<

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

clean:
	rm -r $(BUILD_DIR)

show_vars:
	@printf "CUR_DIR:		$(CUR_DIR)\n\n"
	@printf "BUILD_DIR:		$(BUILD_DIR)\n\n"
	@printf "COMPILE_FLAGS:	$(COMPILE_FLAGS)\n\n"
	@printf "BUILD_FLAGS:	$(BUILD_FLAGS)\n\n"
	@printf "SOURCES:		$(SOURCES)\n\n"
	@printf "OBJECTS:		$(OBJECTS)\n\n"
	@printf "HEADERS:		$(HEADERS)\n\n"
	if [[ $(LD_LIBRARY_PATH) != *$(BUILD_DIR)* ]]; \
	then echo "There are no $(BUILD_DIR) in LD_LIBRARY_PATH yet"; \
	export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(BUILD_DIR); \
	fi