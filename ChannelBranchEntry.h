#include <RtypesCore.h>

struct ChannelBranchEntry {
    Float_t zero_level;
    Short_t min_in_gate;
};