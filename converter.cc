#include <fstream>
#include <sstream>
#include <limits>
#include <cmath>
#include <TTree.h>
#include <TFile.h>
#include "ChannelBranchEntry.h"

// #define DEBUG

short   N_CHANNELS      = 16;
short   N_SAMPLES       = 51;
int     N_FIRST_EVENTS  = 10; 
short (*GATES)[2] = nullptr;

using namespace std;

ifstream txtFile; 
string line, s;
istringstream ss;
short** samples = nullptr;
bool* notEmptyCh = nullptr;
ChannelBranchEntry* br_entries;
TFile rootFile("simpleTree.root", "recreate");
TTree simpleTree("adc64_custom", "adc64_custom");

void init(const char* fname) {
    //open txt data file
    txtFile.open(fname);
    
    //init arrays 
    samples = new short*[N_CHANNELS];
    for (short ch=0; ch<N_CHANNELS; ch++) {
        samples[ch] = new short[N_SAMPLES];
        GATES = new short[N_CHANNELS][2];
    }
    notEmptyCh = new bool[N_CHANNELS];
    br_entries = new ChannelBranchEntry[N_CHANNELS];

    //init tree
    char br_name[11];
    for (short ch=0; ch<N_CHANNELS; ch++) {
        sprintf(br_name, "channel_%d", ch);
        simpleTree.Branch(br_name, &br_entries[ch], "zero_level/F:min_in_gate/S");
    }
}

inline void resetSamples() {
    for (short ch=0; ch<N_CHANNELS; ch++) {
        // for (short si=0; si<N_SAMPLES; si++) samples[ch][si] = 0;
        notEmptyCh[ch] = false;
    }
}


inline int readEvent() {
    bool isSkipping = true;
    int ne;
    short ch, si;
    while (getline(txtFile, line)) {
        ss.str(line);   
        ss.clear();
        ss >> s;
        if (isSkipping) {
            if (!s.compare("Ev:")) {
                isSkipping = false;
                ss >> ne;
                resetSamples();
            }
        } else if (!s.compare("Ev:")) {
            txtFile.seekg(txtFile.tellg()-(streampos)(line.length()+1), txtFile.beg);
            return ne+1;
        } else {
            if (!s.compare("Adc")) {
                ss >> s;
                s.pop_back();
                ch = stoi(s);
                notEmptyCh[ch] = true;
                ss >> s;
                si = 0;
                while (ss >> samples[ch][si++]);
            }
        }    
    }
    return 0;
}

inline void calcInWind(short* min, short* max, float* mean, short ch = 0, short beg = 0, short end = N_SAMPLES-1) {
    *min = numeric_limits<short>::max();
    *max = numeric_limits<short>::min();
    *mean = 0;
    for (short si=beg; si<=end; si++) {
        if (samples[ch][si] > *max) *max = samples[ch][si];
        if (samples[ch][si] < *min) *min = samples[ch][si];
        *mean += samples[ch][si];
    }
    *mean /= (end - beg + 1);
}

inline float calcZeroLevel(const short endSamples[], short ch = 0) {
    float res = 0;
    for (short si=0; si<=endSamples[0]; si++) res += samples[ch][si];
    for (short si=endSamples[1]; si<N_SAMPLES; si++) res += samples[ch][si];
    return res / (N_SAMPLES - endSamples[1] + endSamples[0] + 1);
}

inline float calcRMSout(const short endSamples[], float zlevel, short ch = 0) {
    float res = 0;
    for (short si=0; si<=endSamples[0]; si++) res += (samples[ch][si] - zlevel) * (samples[ch][si] - zlevel);
    for (short si=endSamples[1]; si<N_SAMPLES; si++) res += (samples[ch][si] - zlevel) * (samples[ch][si] - zlevel);
    res /= (N_SAMPLES - endSamples[1] + endSamples[0] + 1);
    return sqrt(res); 
}

inline float calcIntegOut(const short endSamples[], float zlevel, short ch = 0) {
    float res = 0;
    for (short si=0; si<=endSamples[0]; si++) res += abs(samples[ch][si] - zlevel);
    for (short si=endSamples[1]; si<N_SAMPLES; si++) res += abs(samples[ch][si] - zlevel);
    return res;
}

inline float calcIntegIn(const short endSamples[], float zlevel, short ch = 0) {
    float res = 0;
    for (short si=endSamples[0]+1; si<endSamples[1]; si++) res += abs(samples[ch][si] - zlevel);
    return res;
}

inline bool additionalNoiseCheck(const short endSamples[], short halfMax, short ch = 0, bool debug = false) {
    float zlevel = calcZeroLevel(endSamples, ch);
    float rms_out = calcRMSout(endSamples, zlevel, ch);
    bool isSigInv = samples[ch][endSamples[0]] > samples[ch][endSamples[0]+1];
    float shifted = zlevel + ( isSigInv ? -rms_out : rms_out ) * 3;
    // float integOut = calcIntegOut(endSamples, zlevel, ch);
    // float integIn = calcIntegIn(endSamples, zlevel, ch);
#ifdef DEBUG
    if (debug) {
        printf("zlevel: %.2f\trms_out: %.2f\tshifted: %.2f\n", zlevel, rms_out, shifted);
        // printf("integOut: %.2f\tintegIn: %.2f\n", integOut, integIn);
    }
#endif
    return (isSigInv ? shifted < halfMax : shifted > halfMax);// || integOut > integIn;
}

inline bool findHalfMaxSamples(short endSamples[], short ch = 0, bool debug = false) {
    short min, max, halfMax, intersect_i = 0;
    float mean;
    calcInWind(&min, &max, &mean, ch);
    halfMax = (min+max)/2;
    for (short si=0; si<N_SAMPLES-1; si++) {
        if ( (samples[ch][si]-halfMax)*(samples[ch][si+1]-halfMax) <= 0 ) {
            if (intersect_i == 2) return false;
            endSamples[intersect_i++] = si;
        }
    }
    if (intersect_i < 2) return false;
    endSamples[0]++;
#ifdef DEBUG
    if (debug) printf("Half-max: %d\tmin: %d\tmax: %d\n", halfMax, min, max);
#endif
    return !additionalNoiseCheck(endSamples, halfMax, ch, debug);
}


void apprecGates() {
    int ne[N_CHANNELS], ne_raw=0, ne_txt;
    short endSamples[2], actLeft[N_CHANNELS], actRight[N_CHANNELS];
    for (short ch=0; ch<N_CHANNELS; ch++) {
        ne[ch] = 0;
        actLeft[ch] = N_CHANNELS-1;
        actRight[ch] = 0;
    }

    while (ne_txt = readEvent()) {
        for (short ch=0; ch<N_CHANNELS; ch++) 
            if (notEmptyCh[ch]) {
                if ( !findHalfMaxSamples(endSamples, ch/*, ne_txt==63033*/) ) continue;
#ifdef DEBUG
                if (ch == 0 && endSamples[0] == 8) printf("< ev: %d >\n", ne_txt-1);
#endif
                if (endSamples[0] < actLeft[ch]) actLeft[ch] = endSamples[0];
                if (endSamples[1] > actRight[ch]) actRight[ch] = endSamples[1];
                ne[ch]++;  
            }
        if (++ne_raw % N_FIRST_EVENTS == 0) {
            bool isEnough = true;
            for (short ch=0; ch<N_CHANNELS; ch++) 
                if (ne[ch] < N_FIRST_EVENTS) isEnough = false;
            if (isEnough) break;
        }
    }

    for (short ch=0; ch<N_CHANNELS; ch++) {
        GATES[ch][0] = actLeft[ch];
        GATES[ch][1] = actRight[ch];
    }
    txtFile.seekg(0, txtFile.beg);
    txtFile.clear();
    
    printf("First %d events read\n", ne_raw);
    for (short ch=0; ch<N_CHANNELS; ch++) printf("\tch %2d:\tGATE_BEG: %d\tGATE_END: %d\n", ch, GATES[ch][0], GATES[ch][1]);
}


void converter() {
    apprecGates();
    for (short ch=0; ch<N_CHANNELS; ch++)
    {
        GATES[ch][0] = 15;
        GATES[ch][1] = 40;
    }

    int ne;
    short max;
    float mean;
    while (ne = readEvent()) {
        if (ne%10000 == 0) printf("Ev: %d\n", ne);
        for (short ch=0; ch<N_CHANNELS; ch++) {
            br_entries[ch].zero_level = calcZeroLevel(GATES[ch], ch);
            calcInWind(&br_entries[ch].min_in_gate, &max, &mean, ch, GATES[ch][0], GATES[ch][1]);
        }
        simpleTree.Fill();
    }
}
void converter(char const* fname)
{
    init(fname);
    converter();
    simpleTree.Write();
    rootFile.Close();
}

int main(int argc, const char* argv[]) {
    switch (argc) {
        case 2: init(argv[1]); break;
        default: return 1;
    }
    converter();
    simpleTree.Write();
    rootFile.Close();
    return 0;
}