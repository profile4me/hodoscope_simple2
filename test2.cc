#include "../tools/root_scripting.h"
#include <TH2.h>
using namespace root_scripting;

#include "../tools/expr_formatting.h"
using namespace expr_formatting;

#include <iostream>
using namespace std;

#include <TCanvas.h>
#include <TStyle.h>
#include <TROOT.h>

TH1* h;
TH2* h2;

void setStatSize() 
{
    gStyle->SetStatW(0.3);
    gStyle->SetStatH(0.3);
}

void cosmics_vs_Sr90()
{
    //  cosmics vs. Sr90 for 1st bar
    TCanvas canva("canva", "", 1200, 800);
    canva.Divide(3,2);
    addFile("../cosmic_HV44.root");

    canva.cd(1);
    h2 = makeBarAmpH2(0);
    h2->SetTitle("Cosmics: BAR_1"); h2->GetXaxis()->SetTitle("CH0"); h2->GetYaxis()->SetTitle("CH8");
    h2->Draw("colz");

    canva.cd(2);
    h = makeChAmpH(0);
    h->SetTitle("Cosmics: CH0"); h->SetLineColor(2); normalize(h); h->Draw();
    h = makeChAmpH(8);
    h->SetTitle("Cosmics: CH8"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.cd(4);
    switchContext("Sr90_HV44");
    h2 = makeBarAmpH2(0);
    h2->SetTitle("Sr90: BAR_1"); h2->GetXaxis()->SetTitle("CH0"); h2->GetYaxis()->SetTitle("CH8");
    h2->Draw("colz");
    
    canva.cd(5);
    h = makeChAmpH(0);
    h->SetTitle("Sr90: CH0"); h->SetLineColor(2); normalize(h); h->Draw();
    h = makeChAmpH(8);
    h->SetTitle("Sr90: CH8"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.cd(6);
    h = makeBarAmpH(0);
    h->SetTitle("Sr90: (CH0+CH8)/2"); h->SetLineColor(2); normalize(h); h->Draw();
    switchContext("cosmic_HV44");
    h = makeBarAmpH(0);
    h->SetTitle("Cosmics: (CH0+CH8)/2"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.SaveAs("pic/cosmics_vs_Sr90_bar1.png");

 //  cosmics vs. Sr90 for 2nd bar
    canva.cd(1);
    h2 = makeBarAmpH2(1);
    h2->SetTitle("Cosmics: BAR_2"); h2->GetXaxis()->SetTitle("CH1"); h2->GetYaxis()->SetTitle("CH9");
    h2->Draw("colz");

    canva.cd(2);
    h = makeChAmpH(1);
    h->SetTitle("Cosmics: CH1"); h->SetLineColor(2); normalize(h); h->Draw();
    h = makeChAmpH(9);
    h->SetTitle("Cosmics: CH9"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.cd(4);
    switchContext("Sr90_HV44");
    h2 = makeBarAmpH2(1);
    h2->SetTitle("Sr90: BAR_2"); h2->GetXaxis()->SetTitle("CH1"); h2->GetYaxis()->SetTitle("CH9");
    h2->Draw("colz");
    
    canva.cd(5);
    h = makeChAmpH(1);
    h->SetTitle("Sr90: CH1"); h->SetLineColor(2); normalize(h); h->Draw();
    h = makeChAmpH(9);
    h->SetTitle("Sr90: CH9"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.cd(6);
    h = makeBarAmpH(1);
    h->SetTitle("Sr90: (CH1+CH9)/2"); h->SetLineColor(2); normalize(h); h->Draw();
    switchContext("cosmic_HV44");
    h = makeBarAmpH(1);
    h->SetTitle("Cosmics: (CH1+CH9)/2"); h->SetLineColor(3); normalize(h); h->Draw("same");
    gPad->BuildLegend();

    canva.SaveAs("pic/cosmics_vs_Sr90_bar2.png");
}

int main() 
{
//  Different discriminator thresholds
    addFile("../Sr90_HV43_45mV.root");
    h = makeChAmpH(0);
    h->SetTitle("Disc. Thresh.: 45mV");  h->SetLineColor(2); 
    normalize(h);   h->Draw();

    addFile("../Sr90_HV43_50mV.root");
    h = makeChAmpH(0);
    h->SetTitle("Disc. Thresh.: 50mV");  h->SetLineColor(3); 
    normalize(h);   h->Draw("same");

    addFile("../Sr90_HV43_55mV.root");
    h = makeChAmpH(0);
    h->SetTitle("Disc. Thresh.: 55mV");  h->SetLineColor(4); 
    normalize(h);   h->Draw("same");

    gPad->BuildLegend();
    changeTitle("Sr90 HV43_ch0");
    gPad->SaveAs("pic/Sr90_HV43_ch0.png");

//  Different HVs
    addFile("../Sr90_HV43.root");
    h = makeChAmpH(0);
    h->SetTitle("HV: 43");  h->SetLineColor(2); 
    normalize(h);   h->Draw();
    
    addFile("../Sr90_HV44.root");
    h = makeChAmpH(0);
    h->SetTitle("HV: 44");  h->SetLineColor(3); 
    normalize(h);   h->Draw("same");
    
    addFile("../Sr90_HV45.root");
    h = makeChAmpH(0);
    h->SetTitle("HV: 45");  h->SetLineColor(4); 
    normalize(h);   h->Draw("same");
    
    gPad->BuildLegend();
    changeTitle("Sr90_ch0");
    gPad->SaveAs("pic/Sr90_ch0.png");


    cosmics_vs_Sr90();

    clearContexts();
    return 0;
}